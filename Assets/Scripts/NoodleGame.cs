using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NoodleGame : MonoBehaviour
{
    public GameObject cup;
    public GameObject noodleGirl;
    public TextMeshProUGUI debugTimeText;
    public TextMeshProUGUI resultText;
    public Button startButton;
    public Button stopButton;
    public Button restartButton;

    Coroutine timerCoroutine;

    private float currTime = 0f;
    public float maxTime = 20f;

    [System.Serializable]
    public class TimeResult
    {
        public float time;
        public string result;
    }

    public List<TimeResult> timeResults;

    private void Awake()
    {
        startButton.onClick.AddListener( onStartButton);
        stopButton.onClick.AddListener( onStopButton);
        restartButton.onClick.AddListener( onRestartButton);

        setStartState();
    }

    void setStartState()
    {
        currTime = 0f;

        //TODO reset models

        //ui
        debugTimeText.text = "0.0";
        resultText.gameObject.SetActive(false);
        startButton.gameObject.SetActive(true);
        stopButton.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);
        debugTimeText.gameObject.SetActive(false);
        if ( timerCoroutine != null) StopCoroutine(timerCoroutine);
    }

    void setInGameState()
    {
        //TODO animate models

        //ui
        timerCoroutine = StartCoroutine(doTimer());
        startButton.gameObject.SetActive(false);
        stopButton.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(false);
        debugTimeText.gameObject.SetActive(true);
    }

    void setEndGameState()
    {
        debugTimeText.gameObject.SetActive(false);
        resultText.gameObject.SetActive(true);
        startButton.gameObject.SetActive(false);
        stopButton.gameObject.SetActive(false);
        debugTimeText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
        if (timerCoroutine != null)  StopCoroutine(timerCoroutine);

        resultText.text = checkTimeResult( currTime);
    }

    string checkTimeResult( float currTime)
    {
        foreach (TimeResult tr in timeResults)
        {
            if ( currTime < tr.time)
            {
                return tr.result;
            }
        }

        return "";
    }

    public void onStopButton()
    {
        setEndGameState();
    }

    public void onStartButton()
    {
        setInGameState();
    }

    public void onRestartButton()
    {
        setStartState();
    }

    IEnumerator doTimer()
    {
        while( currTime < maxTime)
        {
            yield return new WaitForSeconds(.01f);
            currTime += .01f;
            debugTimeText.text = currTime.ToString();
        }

        setEndGameState();
    }
}
